/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akapon.store;

/**
 *
 * @author Nine
 */
public class Store {

    private String name;
    private String message;
    private int money;
    private int water;
    private int snack;
    private int rice;

    public Store(String name, int money, int water, int snack,
            int rice) {
        this.name = name;
        this.money = money;
        this.water = water;
        this.snack = snack;
        this.rice = rice;
    }

    public void message() {
        System.out.println("Name: " + this.name);
        System.out.println("Have money: " + this.money);
        System.out.println("Water 100 bath | I can buy water:" + this.water);
        System.out.println("Snack 20 bath | I can buy snack:" + this.snack);
        System.out.println("Rice 50 bath | I can buy rice:" + this.rice);
    }

    public double product() {
        return (100 * water) + (20 * this.snack) + (50 * this.rice);
    }

    public void Change() {
        if (this.money < this.product()) {
            System.out.println("not enough money");
        } else {
            System.out.println("Change " + (this.money - this.product()));
        }

    }

    public String getName() {
        return name;
    }

    public int getMoney() {
        return money;
    }

    public int getWater() {
        return water;
    }

    public int getSnack() {
        return snack;
    }

    public int getRice() {
        return rice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public void setSnack(int snack) {

        this.snack = snack;
    }

    public void setRice(int rice) {

        this.rice = rice;
    }

    public void setMoney(int money) {
        this.money = money;

    }
}
